package com.centili.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.centili.entity.DocumentItem;

@Repository
public interface DocumentItemRepository extends CrudRepository<DocumentItem, Long> {
}
