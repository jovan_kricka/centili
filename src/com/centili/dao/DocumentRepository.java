package com.centili.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.centili.entity.Document;

@Repository
public interface DocumentRepository extends CrudRepository<Document, Long> {
}
