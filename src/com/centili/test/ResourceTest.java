package com.centili.test;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.centili.AppConfig;
import com.centili.entity.Document;
import com.centili.entity.DocumentItem;
import com.centili.services.DocumentItemService;
import com.centili.services.DocumentService;
import com.google.common.collect.Lists;
import com.jayway.restassured.RestAssured;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class, loader = AnnotationConfigContextLoader.class)
public abstract class ResourceTest {

	protected static final String CRIME_AND_PUNISHMENT = "Crime and Punishment";
	protected static final String BIG_BANG = "Big Bang";
	protected static final String A_BRIEF_HISTORY_OF_TIME = "A Brief History of Time";
	protected static final String SIDARTA = "Sidarta";
	protected static final String STEPPENWOLF = "Steppenwolf";
	protected static final String THE_HITCHHIKERS_GUIDE_TO_THE_GALAXY = "The Hitchhikers Guide to the Galaxy";

	private static boolean setUpIsDone = false;

	@Autowired
	private DocumentService documentService;
	@Autowired
	private DocumentItemService documentItemService;

	@Before
	public void before() {
		if (setUpIsDone) {
			return;
		}
		initializeRestAssuredPort();
		initializeTestData();
		setUpIsDone = true;
	}

	private void initializeRestAssuredPort() {
		RestAssured.port = 7777;
	}

	private void initializeTestData() {

		List<Document> docs = Lists.newArrayList();
		docs.add(createTestDocument("1", new Date(), THE_HITCHHIKERS_GUIDE_TO_THE_GALAXY));
		docs.add(createTestDocument("2", new Date(), STEPPENWOLF));
		docs.add(createTestDocument("3", new Date(), SIDARTA));
		docs.add(createTestDocument("4", new Date(), A_BRIEF_HISTORY_OF_TIME));
		docs.add(createTestDocument("5", new Date(), BIG_BANG));
		docs.add(createTestDocument("6", new Date(), CRIME_AND_PUNISHMENT));

		for (int i = 0;i < docs.size();i++) {
			createTestDocumentItem(docs.get(i).getName() + " - item 1", 120 + i, docs.get(i));
		}
	}

	private Document createTestDocument(String code, Date date, String name) {
		Document document = new Document();
		document.setCode(code);
		document.setDate(date);
		document.setName(name);
		return documentService.create(document);
	}

	private DocumentItem createTestDocumentItem(String name, double price, Document doc) {
		DocumentItem item = new DocumentItem();
		item.setName(name);
		item.setPrice(price);
		item.setDocument(doc);
		return documentItemService.create(item);
	}
}
