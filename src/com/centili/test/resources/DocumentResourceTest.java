package com.centili.test.resources;

import static com.jayway.restassured.RestAssured.delete;
import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.Test;

import com.centili.test.ResourceTest;
import com.jayway.restassured.http.ContentType;

public class DocumentResourceTest extends ResourceTest {

	private static final String DOCUMENT_JSON = "{\"id\" : \"13\",\"name\" : \"Bible\",\"code\" : \"13\",\"date\" : \"11 11 2011\"}";
	private static final String DOCUMENT_JSON_MODIFIED = "{\"id\" : \"1\",\"name\" : \"Jungle Book\",\"code\" : \"1\",\"date\" : \"11 11 2011\"}";

	@Test
	public void getDocuments_returnAllDocuments_Success() {
		get("/documents/").then().contentType(ContentType.JSON).body("documents.code", hasItems("1", "2", "3", "4", "5", "6")).statusCode(HttpStatus.OK_200);
	}

	@Test
	public void getDocumentById_ReturnJustOneDocument_Success() {
		get("/documents/1").then().contentType(ContentType.JSON).body("code", equalTo("1")).statusCode(HttpStatus.OK_200);
	}

	@Test
	public void getDocumentById_WrongIdFormat_BadRequest() {
		get("/documents/INVALID_ID").then().statusCode(HttpStatus.BAD_REQUEST_400);
	}

	@Test
	public void getDocumentById_NonExistingId_NotFound() {
		get("/documents/7").then().statusCode(HttpStatus.NOT_FOUND_404);
	}

	@Test
	public void postDocument_createDocument_Success() {
		int sizeBefore = get("/documents/").then().contentType(ContentType.JSON).extract().path("documents.size()");
		given().contentType(ContentType.JSON).body(DOCUMENT_JSON).when().post("/documents/").then().statusCode(HttpStatus.OK_200);
		get("/documents/").then().contentType(ContentType.JSON).body("documents", hasSize(sizeBefore + 1)).statusCode(HttpStatus.OK_200);
		get("/documents/13").then().contentType(ContentType.JSON).body("code", equalTo("13")).body("name", equalTo("Bible")).statusCode(HttpStatus.OK_200);
	}

	@Test
	public void putDocument_updateDocument_Success() {
		given().contentType(ContentType.JSON).body(DOCUMENT_JSON_MODIFIED).when().put("/documents/1").then().statusCode(HttpStatus.OK_200);
		get("/documents/1").then().contentType(ContentType.JSON).body("code", equalTo("1")).body("name", equalTo("Jungle Book")).statusCode(HttpStatus.OK_200);
	}

	@Test
	public void putDocument_WrongIdFormat_BadRequest() {
		given().contentType(ContentType.JSON).body(DOCUMENT_JSON_MODIFIED).when().put("/documents/INVALID_ID").then().statusCode(HttpStatus.BAD_REQUEST_400);
	}

	@Test
	public void putDocument_NonExistingId_NotFound() {
		given().contentType(ContentType.JSON).body(DOCUMENT_JSON_MODIFIED).when().put("/documents/100").then().statusCode(HttpStatus.NOT_FOUND_404);
	}

	@Test
	public void deleteDocument_removeDocument_Success() {
		delete("/documents/6").then().statusCode(HttpStatus.OK_200);
		get("/documents/6").then().statusCode(HttpStatus.NOT_FOUND_404);
	}

	@Test
	public void deleteDocument_WrongIdFormat_BadRequest() {
		delete("/documents/INVALID_ID").then().statusCode(HttpStatus.BAD_REQUEST_400);
	}

	@Test
	public void deleteDocument_NonExistingId_NotFound() {
		delete("/documents/100").then().statusCode(HttpStatus.NOT_FOUND_404);
	}

}
