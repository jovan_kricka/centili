package com.centili.test.resources;

import static com.jayway.restassured.RestAssured.delete;
import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.Test;

import com.centili.test.ResourceTest;
import com.jayway.restassured.http.ContentType;

public class DocumentItemResourceTest extends ResourceTest {

	private static final String DOCUMENT_ITEM_JSON = "{\"id\" : \"13\",\"name\" : \"Test Item\",\"price\" : \"126.0\",\"documentId\" : \"1\"}";
	private static final String DOCUMENT_ITEM_JSON_MODIFIED = "{\"id\" : \"7\",\"name\" : \"Test item modified\",\"price\" : \"120.0\",\"documentId\" : \"1\"}";

	@Test
	public void getDocumentItems_returnAllDocumentItems_Success() {
		get("/document_items/").then().contentType(ContentType.JSON).body("documentItems.price", hasItems("120.0", "121.0", "122.0", "123.0", "124.0", "125.0")).statusCode(HttpStatus.OK_200);
	}

	@Test
	public void getDocumentItemById_ReturnJustOneDocumentItem_Success() {
		get("/document_items/7").then().contentType(ContentType.JSON).body("price", equalTo("120.0")).statusCode(HttpStatus.OK_200);
	}

	@Test
	public void getDocumentItemById_WrongIdFormat_BadRequest() {
		get("/document_items/INVALID_ID").then().statusCode(HttpStatus.BAD_REQUEST_400);
	}

	@Test
	public void getDocumentItemById_NonExistingId_NotFound() {
		get("/document_items/13").then().statusCode(HttpStatus.NOT_FOUND_404);
	}

	@Test
	public void postDocumentItem_createDocumentItem_Success() {
		int sizeBefore = get("/document_items/").then().contentType(ContentType.JSON).extract().path("documentItems.size()");
		given().contentType(ContentType.JSON).body(DOCUMENT_ITEM_JSON).when().post("/document_items/").then().statusCode(HttpStatus.OK_200);
		get("/document_items/").then().contentType(ContentType.JSON).body("documentItems", hasSize(sizeBefore + 1)).statusCode(HttpStatus.OK_200);
		get("/document_items/13").then().contentType(ContentType.JSON).body("price", equalTo("126.0")).statusCode(HttpStatus.OK_200);
	}

	@Test
	public void putDocumentItem_updateDocumentItem_Success() {
		given().contentType(ContentType.JSON).body(DOCUMENT_ITEM_JSON_MODIFIED).when().put("/document_items/7").then().statusCode(HttpStatus.OK_200);
		get("/document_items/7").then().contentType(ContentType.JSON).body("price", equalTo("120.0")).body("name", equalTo("Test item modified")).statusCode(HttpStatus.OK_200);
	}

	@Test
	public void putDocumentItem_WrongIdFormat_BadRequest() {
		given().contentType(ContentType.JSON).body(DOCUMENT_ITEM_JSON_MODIFIED).when().put("/document_items/INVALID_ID").then().statusCode(HttpStatus.BAD_REQUEST_400);
	}

	@Test
	public void putDocumentItem_NonExistingId_NotFound() {
		given().contentType(ContentType.JSON).body(DOCUMENT_ITEM_JSON_MODIFIED).when().put("/documents/100").then().statusCode(HttpStatus.NOT_FOUND_404);
	}

	@Test
	public void deleteDocumentItem_removeDocumentItem_Success() {
		delete("/document_items/12").then().statusCode(HttpStatus.OK_200);
		get("/documents/12").then().statusCode(HttpStatus.NOT_FOUND_404);
	}

	@Test
	public void deleteDocumentItem_WrongIdFormat_BadRequest() {
		given().contentType(ContentType.JSON).body(DOCUMENT_ITEM_JSON_MODIFIED).when().delete("/documents/INVALID_ID").then().statusCode(HttpStatus.BAD_REQUEST_400);
	}

	@Test
	public void deleteDocumentItem_NonExistingId_NotFound() {
		given().contentType(ContentType.JSON).body(DOCUMENT_ITEM_JSON_MODIFIED).when().delete("/documents/100").then().statusCode(HttpStatus.NOT_FOUND_404);
	}

}
