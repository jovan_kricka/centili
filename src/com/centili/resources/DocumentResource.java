package com.centili.resources;

import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

import org.eclipse.jetty.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.centili.adapters.DocumentAdapter;
import com.centili.dto.DocumentDto;
import com.centili.dto.DocumentsDto;
import com.centili.entity.Document;
import com.centili.services.DocumentService;
import com.google.common.collect.ImmutableList;
import com.google.common.net.MediaType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
public class DocumentResource {

	@Autowired
	private DocumentService documentService;

	public DocumentResource() {
		initializeResource();
	}

	public void initializeResource() {

		// Gson for Json transformation
		Gson gson = new GsonBuilder().setDateFormat("dd MM yyyy").create();

		get("/documents/", (request, response) -> {
			ImmutableList<Document> documents = documentService.getAll();
			ImmutableList<DocumentDto> dtos = DocumentAdapter.entitiesToModels(documents);
			response.type(MediaType.JSON_UTF_8.toString());
			return new DocumentsDto(dtos);
		}, gson::toJson);

		get("/documents/:id", (request, response) -> {
			String id = request.params(":id");
			try {
				long idLong = Long.parseLong(id);
				Document doc = documentService.getById(idLong);
				if (doc == null) {
					response.status(HttpStatus.NOT_FOUND_404);
					return HttpStatus.NOT_FOUND_404;
				} else {
					response.type(MediaType.JSON_UTF_8.toString());
					return DocumentAdapter.entityToModel(doc);
				}
			} catch (NumberFormatException e) {
				response.status(HttpStatus.BAD_REQUEST_400);
				return HttpStatus.BAD_REQUEST_400;
			}
		}, gson::toJson);

		post("/documents/", MediaType.JSON_UTF_8.toString(), (request, response) -> {
			String jsonBody = request.body();
			DocumentDto dto = gson.fromJson(jsonBody, DocumentDto.class);
			Document doc = DocumentAdapter.modelToEntity(dto);
			doc.setId(null);
			documentService.create(doc);
			return HttpStatus.OK_200;
		});

		put("/documents/:id", MediaType.JSON_UTF_8.toString(), (request, response) -> {
			String id = request.params(":id");
			try {
				long idLong = Long.parseLong(id);
				if (documentService.exists(idLong)) {
					String jsonBody = request.body();
					Document doc = gson.fromJson(jsonBody, Document.class);
					doc.setId(idLong);
					documentService.update(doc);
					return HttpStatus.OK_200;
				} else {
					response.status(HttpStatus.NOT_FOUND_404);
					return HttpStatus.NOT_FOUND_404;
				}
			} catch (NumberFormatException e) {
				response.status(HttpStatus.BAD_REQUEST_400);
				return HttpStatus.BAD_REQUEST_400;
			}
		});

		delete("/documents/:id", (request, response) -> {
			String id = request.params(":id");
			try {
				long idLong = Long.parseLong(id);
				if (documentService.exists(idLong)) {
					documentService.delete(idLong);
					return HttpStatus.OK_200;
				} else {
					response.status(HttpStatus.NOT_FOUND_404);
					return HttpStatus.NOT_FOUND_404;
				}
			} catch (NumberFormatException e) {
				response.status(HttpStatus.BAD_REQUEST_400);
				return HttpStatus.BAD_REQUEST_400;
			}
		});
	}

}
