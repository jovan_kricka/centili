package com.centili.resources;

import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

import org.eclipse.jetty.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.centili.adapters.DocumentItemAdapter;
import com.centili.dto.DocumentItemDto;
import com.centili.dto.DocumentItemsDto;
import com.centili.entity.DocumentItem;
import com.centili.services.DocumentItemService;
import com.centili.services.DocumentService;
import com.google.common.collect.ImmutableList;
import com.google.common.net.MediaType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
public class DocumentItemResource {

	@Autowired
	private DocumentItemService documentItemService;
	@Autowired
	private DocumentService documentService;

	public DocumentItemResource() {
		initializeResource();
	}

	public void initializeResource() {

		// Gson for Json transformation
		Gson gson = new GsonBuilder().setDateFormat("dd MMM yyyy").create();

		get("/document_items/", (request, response) -> {
			ImmutableList<DocumentItem> documentItems = documentItemService.getAll();
			ImmutableList<DocumentItemDto> dtos = DocumentItemAdapter.entitiesToModels(documentItems);
			response.type(MediaType.JSON_UTF_8.toString());
			return new DocumentItemsDto(dtos);
		}, gson::toJson);

		get("/document_items/:id", (request, response) -> {
			String id = request.params(":id");
			try {
				Long idLong = Long.parseLong(id);
				DocumentItem docItem = documentItemService.getById(idLong);
				if (docItem == null) {
					response.status(HttpStatus.NOT_FOUND_404);
					return HttpStatus.NOT_FOUND_404;
				} else {
					response.type(MediaType.JSON_UTF_8.toString());
					DocumentItemDto dto = DocumentItemAdapter.entityToModel(docItem);
					return dto;
				}
			} catch (NumberFormatException e) {
				response.status(HttpStatus.BAD_REQUEST_400);
				return HttpStatus.BAD_REQUEST_400;
			}
		}, gson::toJson);

		get("/documents/:id/document_items/", (request, response) -> {
			String documentId = request.params(":id");
			try {
				Long documentIdLong = Long.parseLong(documentId);
				if (documentService.exists(documentIdLong)) {
					ImmutableList<DocumentItem> items = documentItemService.getByDocumentId(documentIdLong);
					ImmutableList<DocumentItemDto> dtos = DocumentItemAdapter.entitiesToModels(items);
					response.type(MediaType.JSON_UTF_8.toString());
					return new DocumentItemsDto(dtos);
				} else {
					response.status(HttpStatus.NOT_FOUND_404);
					return HttpStatus.NOT_FOUND_404;
				}
			} catch (NumberFormatException e) {
				response.status(HttpStatus.BAD_REQUEST_400);
				return HttpStatus.BAD_REQUEST_400;
			}
		}, gson::toJson);

		post("/document_items/", MediaType.JSON_UTF_8.toString(), (request, response) -> {
			String jsonBody = request.body();
			DocumentItemDto dto = gson.fromJson(jsonBody, DocumentItemDto.class);
			DocumentItem docItem = DocumentItemAdapter.modelToEntity(dto);
			docItem.setId(null);
			documentItemService.create(docItem);
			return HttpStatus.OK_200;
		});

		put("/document_items/:id", MediaType.JSON_UTF_8.toString(), (request, response) -> {
			String id = request.params(":id");
			try {
				long idLong = Long.parseLong(id);
				if (documentItemService.exists(idLong)) {
					String jsonBody = request.body();
					DocumentItemDto dto = gson.fromJson(jsonBody, DocumentItemDto.class);
					DocumentItem item = DocumentItemAdapter.modelToEntity(dto);
					documentItemService.update(item);
					return HttpStatus.OK_200;
				} else {
					response.status(HttpStatus.NOT_FOUND_404);
					return HttpStatus.NOT_FOUND_404;
				}
			} catch (NumberFormatException e) {
				response.status(HttpStatus.BAD_REQUEST_400);
				return HttpStatus.BAD_REQUEST_400;
			}
		});

		delete("/document_items/:id", (request, response) -> {
			String id = request.params(":id");
			try {
				long idLong = Long.parseLong(id);
				if (documentItemService.exists(idLong)) {
					documentItemService.delete(idLong);
					return HttpStatus.OK_200;
				} else {
					response.status(HttpStatus.NOT_FOUND_404);
					return HttpStatus.NOT_FOUND_404;
				}
			} catch (NumberFormatException e) {
				response.status(HttpStatus.BAD_REQUEST_400);
				return HttpStatus.BAD_REQUEST_400;
			}
		});
	}

}
