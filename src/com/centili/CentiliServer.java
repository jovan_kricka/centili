package com.centili;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CentiliServer {

	public static ApplicationContext springContext;

	public static void main(String[] args) {
		initializeSpringContext();
	}

	private static void initializeSpringContext() {
		springContext = new AnnotationConfigApplicationContext(AppConfig.class);
	}

}
