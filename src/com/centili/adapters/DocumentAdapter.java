package com.centili.adapters;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.centili.dto.DocumentDto;
import com.centili.entity.Document;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

public class DocumentAdapter {

	public static ImmutableList<DocumentDto> entitiesToModels(Iterable<Document> all) {
		Builder<DocumentDto> builder = new ImmutableList.Builder<DocumentDto>();
		for (Document doc : all) {
			DocumentDto dto = entityToModel(doc);
			builder.add(dto);
		}
		return builder.build();
	}

	public static DocumentDto entityToModel(Document doc) {
		DocumentDto dto = new DocumentDto();
		dto.setId(doc.getId().toString());
		dto.setCode(doc.getCode());
		dto.setName(doc.getName());
		dto.setDate(doc.getDate().toString());
		return dto;
	}

	public static Document modelToEntity(DocumentDto dto) throws ParseException {
		Document doc = new Document();
		doc.setId(Long.parseLong(dto.getId()));
		doc.setCode(dto.getCode());
		doc.setName(dto.getName());
		doc.setDate(new SimpleDateFormat("dd MM YYYY").parse(dto.getDate()));
		return doc;
	}

}
