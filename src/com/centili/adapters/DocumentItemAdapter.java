package com.centili.adapters;

import com.centili.dto.DocumentItemDto;
import com.centili.entity.Document;
import com.centili.entity.DocumentItem;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

public class DocumentItemAdapter {

	public static ImmutableList<DocumentItemDto> entitiesToModels(Iterable<DocumentItem> all) {
		Builder<DocumentItemDto> builder = new ImmutableList.Builder<DocumentItemDto>();
		for (DocumentItem doc : all) {
			DocumentItemDto dto = entityToModel(doc);
			builder.add(dto);
		}
		return builder.build();
	}

	public static DocumentItemDto entityToModel(DocumentItem docItem) {
		DocumentItemDto dto = new DocumentItemDto();
		dto.setId(docItem.getId().toString());
		dto.setName(docItem.getName());
		dto.setPrice(docItem.getPrice().toString());
		dto.setDocumentId(docItem.getDocument().getId().toString());
		return dto;
	}

	public static DocumentItem modelToEntity(DocumentItemDto dto) {
		DocumentItem docItem = new DocumentItem();
		docItem.setId(Long.parseLong(dto.getId()));
		docItem.setName(dto.getName());
		docItem.setPrice(Double.valueOf(dto.getPrice()));
		Document doc = new Document();
		doc.setId(Long.parseLong(dto.getDocumentId()));
		docItem.setDocument(doc);
		return docItem;
	}

}
