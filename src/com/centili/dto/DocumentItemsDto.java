package com.centili.dto;

import com.google.common.collect.ImmutableList;

public class DocumentItemsDto {

	private ImmutableList<DocumentItemDto> documentItems;

	public DocumentItemsDto() {
	}

	public DocumentItemsDto(ImmutableList<DocumentItemDto> documentItems) {
		this.documentItems = documentItems;
	}

	public ImmutableList<DocumentItemDto> getDocumentItems() {
		return documentItems;
	}

	public void setDocumentItems(ImmutableList<DocumentItemDto> documentItems) {
		this.documentItems = documentItems;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((documentItems == null) ? 0 : documentItems.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentItemsDto other = (DocumentItemsDto) obj;
		if (documentItems == null) {
			if (other.documentItems != null)
				return false;
		} else if (!documentItems.equals(other.documentItems))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DocumentItemsDto [documentItems=" + documentItems + "]";
	}

}
