package com.centili.dto;

import com.google.common.collect.ImmutableList;

public class DocumentsDto {

	private ImmutableList<DocumentDto> documents;

	public DocumentsDto() {
	}

	public DocumentsDto(ImmutableList<DocumentDto> documents) {
		this.documents = documents;
	}

	public ImmutableList<DocumentDto> getDocuments() {
		return documents;
	}

	public void setDocuments(ImmutableList<DocumentDto> documents) {
		this.documents = documents;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((documents == null) ? 0 : documents.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentsDto other = (DocumentsDto) obj;
		if (documents == null) {
			if (other.documents != null)
				return false;
		} else if (!documents.equals(other.documents))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DocumentsDto [documents=" + documents + "]";
	}

}
