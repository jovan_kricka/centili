package com.centili.services;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.centili.dao.DocumentItemRepository;
import com.centili.dao.DocumentRepository;
import com.centili.entity.Document;
import com.centili.entity.DocumentItem;
import com.google.common.collect.ImmutableList;

@Component
@Transactional
public class DocumentItemService {

	private final DocumentItemRepository documentItemRepository;
	private final DocumentRepository documentRepository;

	@Autowired
	public DocumentItemService(DocumentItemRepository documentItemRepository, DocumentRepository documentRepository) {
		this.documentItemRepository = documentItemRepository;
		this.documentRepository = documentRepository;
	}

	@Transactional
	public ImmutableList<DocumentItem> getAll() {
		return ImmutableList.copyOf(documentItemRepository.findAll());
	}

	@Transactional
	public DocumentItem getById(Long id) {
		return documentItemRepository.findOne(id);
	}

	@Transactional
	public ImmutableList<DocumentItem> getByDocumentId(Long documentId) {
		Document document = documentRepository.findOne(documentId);
		Set<DocumentItem> items = document.getItems();
		return ImmutableList.copyOf(items);
	}

	@Transactional
	public DocumentItem create(DocumentItem docItem) {
		return documentItemRepository.save(docItem);
	}

	@Transactional
	public DocumentItem update(DocumentItem docItem) {
		return documentItemRepository.save(docItem);
	}

	@Transactional
	public boolean exists(long id) {
		return documentItemRepository.findOne(id) != null;
	}

	@Transactional
	public void delete(long id) {
		documentItemRepository.delete(id);
	}

}
