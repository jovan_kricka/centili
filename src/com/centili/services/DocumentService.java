package com.centili.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.centili.dao.DocumentRepository;
import com.centili.entity.Document;
import com.google.common.collect.ImmutableList;

@Component
@Transactional
public class DocumentService {

	private final DocumentRepository documentRepository;

	@Autowired
	public DocumentService(DocumentRepository documentRepository) {
		this.documentRepository = documentRepository;
	}

	@Transactional
	public ImmutableList<Document> getAll() {
		Iterable<Document> all = documentRepository.findAll();
		return ImmutableList.copyOf(all);
	}

	@Transactional
	public Document getById(long id) {
		return documentRepository.findOne(id);
	}

	@Transactional
	public Document create(Document document) {
		return documentRepository.save(document);
	}

	public Document update(Document document) {
		return documentRepository.save(document);
	}

	@Transactional
	public void delete(Long id) {
		documentRepository.delete(id);
	}

	@Transactional
	public boolean exists(Long id) {
		return documentRepository.findOne(id) != null;
	}

}
